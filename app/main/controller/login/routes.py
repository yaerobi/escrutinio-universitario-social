from flask import Blueprint, render_template, request, redirect, url_for
from app.main import flask_bcrypt
from app.main.services.users_services import get_user
from app.main.model.users import Users

login_blueprint = Blueprint('login', __name__, template_folder="templates")


@login_blueprint.route("/admin_login", methods=("GET", "POST"))
def login():
    if request.method == "GET":
        return render_template("login.html")
    elif request.method == "POST":
        username = request.form.get('username')

        user = get_user(username)  # type: Users
        if not user:
            print("not user")
            return render_template("login.html")

        if flask_bcrypt.check_password_hash(user.password, request.form.get('password')):
            return redirect(url_for('admin.admin'))
        else:
            print("password nok")

        return render_template("login.html")
    else:
        return {"msg": "What are you doing here?"}, 404

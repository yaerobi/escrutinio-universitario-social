from flask import Blueprint, render_template, request, redirect, url_for
from app.main import flask_bcrypt
from app.main.services.users_services import get_user
from app.main.model.users import Users
from flask_table import Table, Col


admin_blueprint = Blueprint('admin', __name__, template_folder="templates")


@admin_blueprint.route("/", methods=("GET", "POST"))
def admin():
    if request.method == "POST":
        if request.form.get("testigo"):
            return redirect(url_for('admin.vote_testigo'))
    return render_template("admin.html")


class TestigoTable(Table):
    dni = Col('DNI')
    name = Col('Nombre')
    surname = Col('Apellido')
    email = Col('email')
    phone = Col('Celular')
    estamento = Col('Estamento')
    sede = Col('Sede')

"""
@admin_blueprint.route("/vote_testigo")
def vote_testigo():
    number_of_voted = get_number_voted()
    list_not_voted = TestigoTable(get_all_not_voted())

    return render_template("vote_testigo.html", number_of_voted=number_of_voted, list_not_voted=list_not_voted)

@admin_blueprint.route("/elecciones")
def vote_testigo():
    render_template("elecciones.html")
    
    
    estamentos = [{"name": "Graduados"},
                  {"name": "No Docentes"},
                  {"name": "Docentes"},
                  {"name": "Estudiantes"}]
    
    op.bulk_insert(estamento, estamentos)
"""

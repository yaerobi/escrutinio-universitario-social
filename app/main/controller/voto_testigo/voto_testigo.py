from flask_restful import Resource, reqparse
from app.main.model.persons import Persons
from typing import List
from app.main.services.persons_services import (
    get_all_persons,
    get_all_voted_persons,
    get_all_not_voted_persons,
    get_persons_by,
)
from app.main.services.voting_services import count_voted


def get_persons_data(persons: List[Persons]):
    return [persona.as_dict() for persona in persons]


class Persons(Resource):
    def get(self):
        persons = get_all_persons()
        return get_persons_data(persons), 200


class PersonsVoted(Resource):
    def get(self):
        persons = get_all_voted_persons()
        return get_persons_data(persons), 200


class PersonsNotVoted(Resource):
    def get(self):
        persons = get_all_not_voted_persons()
        return get_persons_data(persons), 200


class Filters(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("mesa", type=int, required=False)
        parser.add_argument("n_orden", type=int, required=False)
        parser.add_argument("dni", type=str, required=False)
        parser.add_argument("estamento", type=str, required=False)
        parser.add_argument("depto_sede", type=str, required=False)

        args = parser.parse_args()
        return get_persons_data(get_persons_by(**args)), 200


class CountVoted(Resource):
    def get(self):
        number_voter = count_voted()
        return {"votaron": number_voter}, 200



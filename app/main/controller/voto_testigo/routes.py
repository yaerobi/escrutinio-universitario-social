from flask import Blueprint
from flask_restful import Api
from app.main.controller.voto_testigo.voto_testigo import (
    Persons,
    PersonsVoted,
    PersonsNotVoted,
    Filters,
    CountVoted,
)


api_voted_blueprint = Blueprint('api_voted', __name__)

api = Api(api_voted_blueprint, "/api_voted/v1")

# registering
api.add_resource(Persons, '/persons')
api.add_resource(PersonsVoted, '/persons_voted')
api.add_resource(PersonsNotVoted, '/persons_not_voted')
api.add_resource(Filters, '/filters_by')
api.add_resource(CountVoted, '/vote/count')

from app.main import db


def add(entity):
    db.session.add(entity)
    db.session.commit()


def remove(entity):
    db.session.remove(entity)
    db.session.commit()

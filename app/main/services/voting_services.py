from app.main import db
from app.main.model.persons import Persons
from app.main.model.mesas import Mesas


def vote(mesa: int, n_orden: int) -> Persons:
    mesa = Mesas.query.filter_by(num=mesa).first()
    persona = Persons.query.filter_by(mesa=mesa, n_orden=n_orden)
    persona.vote = True
    db.session.commit()
    return persona


def cancel_vote(mesa: int, n_orden: int) -> Persons:
    mesa = Mesas.query.filter_by(num=mesa).first()
    persona = Persons.query.filter_by(mesa=mesa, n_orden=n_orden)
    persona.vote = False
    db.session.commit()
    return persona


def count_voted() -> int:
    persona = Persons.query.filter_by(vote=True)
    return len(persona)


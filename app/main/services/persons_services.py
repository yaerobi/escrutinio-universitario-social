import itertools


from app.main.model.estamentos import Estamentos
from app.main.model.persons import Persons
from app.main.model.mesas import Mesas
from typing import List, Optional


def get_all_persons() -> List[Persons]:
    return Persons.query.all()


def get_all_voted_persons() -> List[Persons]:
    return Persons.query.filter_by(vote=True)


def get_all_not_voted_persons() -> List[Persons]:
    return Persons.query.filter_by(vote=False)


def get_persons_by(mesa: Optional[int] = None, n_orden: Optional[int] = None,
                   dni: Optional[str] = None, estamento: Optional[str] = None,
                   depto_sede: Optional[str] = None) -> List[Persons]:

    if estamento is not None:
        esta = Estamentos.query.filter_by(name=estamento).first()
        mesa = Mesas.query.filter_by(estamento=esta).all()
        persons = [Persons.query.filter_by(mesa=m).all() for m in mesa]
        return list(itertools.chain.from_iterable(persons))

    if depto_sede is not None:
        mesas = Mesas.query.filter_by(depto_sede=depto_sede).all()
        persons = [Persons.query.filter_by(mesa=m).all() for m in mesas]
        return list(itertools.chain.from_iterable(persons))

    query_filter = {}

    if mesa is not None:
        query_filter.update({"mesa": Mesas.query.filter_by(num=mesa).first()})
    if n_orden is not None:
        query_filter.update({"n_orden": n_orden})
    if dni is not None:
        query_filter.update({"dni": dni})

    return Persons.query.filter_by(**query_filter).all()

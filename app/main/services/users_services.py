from app.main.model.users import Users


def get_user(username: str):
    user = Users.query.filter_by(name=username).first()
    return user

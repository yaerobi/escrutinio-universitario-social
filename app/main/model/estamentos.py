from app.main import db


class Estamentos(db.Model):
    __tablename__ = 'estamentos'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(255), nullable=False)

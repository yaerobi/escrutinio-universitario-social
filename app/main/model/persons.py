import os
import csv

from pathlib import Path
from app.main import db
from app.main.model.mesas import Mesas
from dataclasses import dataclass


@dataclass
class PersonaRow:
    mesa: str
    n_orden:str
    name: str
    tipo_dni: str
    dni: str
    vot_rector: str

    @property
    def vota_rector(self):
        if self.vot_rector.upper() == "SI":
            return True
        return False


class Persons(db.Model):
    __tablename__ = 'persons'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    n_orden = db.Column(db.Integer, nullable=False)
    tipo_dni = db.Column(db.String(10), nullable=False)
    dni = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255), nullable=False)
    vota_rector = db.Column(db.Boolean, nullable=False)
    vote = db.Column(db.Boolean, default=False, nullable=False)

    # relationships
    mesa_id = db.Column(db.Integer, db.ForeignKey('mesas.id'),
                        nullable=False)
    mesa = db.relationship("Mesas", backref="persons", lazy=True)

    def __init__(self, dni, n_orden, name, tipo_dni, vota_rector):
        self.dni = dni
        self.n_orden = n_orden
        self.name = name
        self.tipo_dni = tipo_dni
        self.vota_rector = vota_rector

    def as_dict(self):
        return {
            "n_orden": self.n_orden,
            "dni": self.dni,
            "name": self.name,
            "mesa": self.mesa.num,
            "estamento": self.mesa.estamento.name,
            "depto_sede": self.mesa.depto_sede,
            "vota_rector": self.vota_rector,
            "voto": self.vote,
        }

    @staticmethod
    def populate_pesonas():
        data_path = os.getenv("DATA_PATH")

        # csv_files = ["docentes.csv", "estudiantes.csv", "nodocente.csv"]
        csv_files = ["nodocentes.csv"]

        for csv_file in csv_files:
            path_csv = Path(data_path) / Path(csv_file)
            Persons._populate(path_csv)

    @staticmethod
    def _populate(data_raw: Path):
        with data_raw.open() as csv_file:
            csv_data = csv.reader(csv_file)
            for csv_row in csv_data:
                persona_row = PersonaRow(*csv_row)

                persons = Persons(persona_row.dni, persona_row.n_orden, persona_row.name,
                                  persona_row.tipo_dni, persona_row.vota_rector)

                mesa = Mesas.query.filter_by(num=int(persona_row.mesa)).first()
                persons.mesa = mesa

                db.session.add(persons)
        db.session.commit()
        print(f"finish {str(data_raw)}")


import os
import csv

from pathlib import Path
from dataclasses import dataclass
from app.main import db
from app.main.model.estamentos import Estamentos
from typing import Dict, List, Optional


@dataclass
class MesasRow:
    num: str
    ciudad: str
    lugar: Optional[str]
    letras: Optional[str]
    estamento: str
    depto_sede: str
    desde: str
    hasta: str
    total: str


class Mesas(db.Model):
    __tablename__ = 'mesas'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    num = db.Column(db.Integer, nullable=False)
    ciudad = db.Column(db.String(50), nullable=False)
    lugar = db.Column(db.String(255), nullable=True)
    letras = db.Column(db.String(255), nullable=True)
    depto_sede = db.Column(db.String(50), nullable=True)
    desde = db.Column(db.Integer, nullable=False)
    hasta = db.Column(db.Integer, nullable=False)
    total = db.Column(db.Integer, nullable=False)

    # relationships
    estamento_id = db.Column(db.Integer, db.ForeignKey('estamentos.id'),
        nullable=False)
    estamento = db.relationship("Estamentos", backref="mesas", lazy=True)

    def __init__(self, num, ciudad, lugar, letras, depto_sedes, desde, hasta, total):
        self.num = num
        self.ciudad = ciudad
        self.lugar = lugar
        self.letras = letras
        self.depto_sede = depto_sedes
        self.desde = desde
        self.hasta = hasta
        self.total = total

    def as_dict(self):
        return {
            "n_mesa": self.num,
            "estamento": Estamentos.query.filter_by(id=self.estamento_id).first().name,
            "seccion": self.seccion,
        }

    @staticmethod
    def populate_mesas():
        data_path = os.getenv("DATA_PATH")
        path_csv = Path(data_path) / Path("mesas.csv")

        with path_csv.open() as csv_file:
            csv_data = csv.reader(csv_file)
            for csv_row in csv_data:
                mesa_row = MesasRow(*csv_row)
                mesa = Mesas(mesa_row.num, mesa_row.ciudad, mesa_row.lugar, mesa_row.letras,
                             mesa_row.depto_sede, int(mesa_row.desde), int(mesa_row.hasta),
                             int(mesa_row.total))

                estamento = Estamentos.query.filter_by(name=mesa_row.estamento).first()
                mesa.estamento = estamento

                db.session.add(mesa)
        db.session.commit()

import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from .config import config_by_name

db = SQLAlchemy()
flask_bcrypt = Bcrypt()


def create_app():
    app = Flask(__name__)
    app.config.from_object(config_by_name[os.getenv('FLASK_ENV') or 'dev'])
    db.init_app(app)
    flask_bcrypt.init_app(app)
    app.config["JWT_SECRET_KEY"] = "super-secret"  # TODO: Change this before production
    jwt = JWTManager(app)

    # Migrate database
    migrate = Migrate(app, db)

    # register blueprints
    from app.main.controller.voto_testigo.routes import api_voted_blueprint
    from app.main.controller.login.routes import login_blueprint
    from app.main.controller.admin.routes import admin_blueprint

    app.register_blueprint(api_voted_blueprint)
    app.register_blueprint(login_blueprint, url_prefix='/login')
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    return app
